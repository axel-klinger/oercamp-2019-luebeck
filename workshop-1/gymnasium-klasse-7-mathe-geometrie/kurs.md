---
schultyp: 'Gymnasium'
jahrgang: '7'
fach: 'Mathematik'
thema: 'Geometrie'
---

# Geometrie der Dreiecke

* Wofür? -> Anwendungsbeispiele
* Was ist bekannt und wie funktioniert es? -> Erläuterungen
* Was ist noch offen? -> ... ggf.
* Aufgaben zum LERNEN!

## Wiederholung Dreiecke allgemein

* Winkelsummen etc

## Der Satz des Pythagoras

[Satz des Pythagoras](satz-des-pythagoras.md)

## Der Höhensatz des Euklid

[Höhensatz des Euklid](hoehensatz-des-euklid.md)
