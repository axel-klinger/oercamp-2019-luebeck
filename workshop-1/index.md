# Kurse mit GitLab

GitLab bietet ...

## Anmelden / Registrieren

* [Anmelden / Registrieren](https://gitlab.com/users/sign_in)
* Übersicht

## Projekt erstellen

* Name des Projekts vs.
* Seite des Projekts
* Public / Private
* README gleich miterstellen
* Lizenz manuell einfügen (derzeit noch keine CC-Vorlagen)

## Inhalte verfassen

* Datei für Kurs erstellen
* Dateien für Module erstellen
* [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)

## Kurs veröffentlichen

* Settings -> ...

## Fremden Kurs forken

* ...

## Fremden Kurs bearbeiten und Änderung dem Autor empfehlen



## Änderungen annehmen oder ablehnen



## Gemeinsames Arbeiten am Kurs

* Anderen GitLab-Usern Zugriff auf Projekt gewähren
