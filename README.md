# OERcamp 2019 Lübeck

Alle Unterlagen zu den Workshops rund um Git basierte OER.

## Workshop 1 : "Gemeinsame Erstellung und Bearbeitung von freien Lehr- und Lernmaterialien in GitLab"

**Worum geht’s?**
Für die Bearbeitung von freien Lehr- und Lernmaterialien auf Basis von Texten, in denen Bilder, Tabellen, Videos, Aufgaben u.v.a.m. integriert werden können, bietet GitLab zahlreiche Vorteile. Dieser Workshop demonstriert den Einsatz von GitLab für freie Bildungsmaterialien.

**Bildungsbereich :** übergreifend

* [Präsentation](https://axel-klinger.gitlab.io/oer-slides/oer-ws-1.html)
* [Inhalt](workshop-1/index.md)
* [Beispiel Kurs Schule](workshop-1/gymnasium-klasse-7-mathe-geometrie/kurs.md)
* [Beispiel Kurs Uni](workshop-1/uni-bauing-semester-3-statik/statik-3.md)

## Workshop 2 : "Einen ganzen Studiengang (Uni) / Jahrgang (Schule) mit GitLab abbilden…"

**Worum geht’s?**
… und mit geteilter Verantwortung verwalten. Neben einzelnen Kursen und
Modulen lassen sich auch ganze Studiengänge oder Jahrgänge in GitLab
abbilden. In diesem Workshop wird die Verwaltung der Kurse einer
Lehreinrichtung in GitLab exemplarisch umgesetzt.

**Bildungsbereich :** übergreifend

* [Präsentation](https://axel-klinger.gitlab.io/oer-slides/oer-ws-2.html)
* [Inhalt](workshop-2/index.md)
* [Beispiel Studiengang Uni](https://gitlab.com/axel-klinger/mein-studiengang/blob/master/studiengang.md)
* [Beispiel Jahrgänge Schule](https://gitlab.com/axel-klinger/meine-schule/blob/master/schule.md)
